const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

console.log(productControllers);



router.post("/", auth.verify, productControllers.addProduct);


router.get("/all", auth.verify, productControllers.getAllProducts);


router.get("/", productControllers.getAllActive);


router.get("/:productId", productControllers.getProduct);


router.put("/:productId", auth.verify, productControllers.updateProduct);


router.patch("/archive/:productId", auth.verify, productControllers.archiveProduct);







module.exports = router;