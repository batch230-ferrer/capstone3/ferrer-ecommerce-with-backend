const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

console.log(userControllers);


router.post("/checkEmail", userControllers.checkEmailExists);


router.post("/register", userControllers.registerUser);


router.get("/all", userControllers.getAllUsers);


router.post("/login", userControllers.loginUser);


router.get("/details", auth.verify, userControllers.getProfile);

router.get("/:userId", userControllers.getProfile);


router.post("/checkout", auth.verify, userControllers.checkOut);


module.exports = router;