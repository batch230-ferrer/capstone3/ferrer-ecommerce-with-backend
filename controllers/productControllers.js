const Product = require("../models/Product");
const auth = require("../auth");


module.exports.addProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	let newProduct = new Product({
		name : req.body.name,
		description : req.body.description,
		price : req.body.price,
		stocks: req.body.stocks
	});

	if(req.userData.isAdmin){

	
		return newProduct.save()
		.then(product => {
			console.log(product);
			res.send(true)
		})
		
		.catch(error => {
			console.log(error);
			res.send(false);
		});
	}
	else {
		
		return res.status(401).send("You don't have access to this page!");
	};

};


module.exports.getAllProducts = (req, res) =>{
	
	

	if(req.userData.isAdmin){
		return Product.find({}).then(result => res.send(result));
	}
	else{
		return res.send(false);
		
	}
}


module.exports.getAllActive = (req, res) =>{
	return Product.find({isActive: true}).then(result => res.send(result));
}


module.exports.getProduct = (req, res) =>{
	console.log(req.params.productId);

	return Product.findById(req.params.productId).then(result => res.send(result));
}



module.exports.updateProduct = (req, res) =>{
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		let updateProduct = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			stocks: req.body.stocks
		}

		return Product.findByIdAndUpdate(req.params.productId, updateProduct, {new:true})
		.then(result =>{
			console.log(result);
			res.send(result);
		})
		.catch(error =>{
			console.log(error);
			res.send(false);
		});
	}
	else{
		
		return res.status(401).send("You don't have access to this page!");
	}
}



module.exports.archiveProduct = (req, res) =>{

	const userData = auth.decode(req.headers.authorization);

	let updateIsActiveField = {
		isActive: req.body.isActive
	}

	if(userData.isAdmin){
		return Product.findByIdAndUpdate(req.params.productId, updateIsActiveField)
		.then(result => {
			console.log(result);
			res.send(true);
		})
		.catch(error =>{
			console.log(error);
			res.send(false);
		})
	}
	else{
		return res.send(false);
	}
}

