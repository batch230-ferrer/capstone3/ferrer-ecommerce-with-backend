const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

// Allows our backend application to be available with our frontend application.
	// Cross Origin Resource Sharing
const cors = require("cors");

const app = express();

// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.ebpzzsg.mongodb.net/courseBooking?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

/*mongoose.connect("mongodb+srv://admin:admin@batch230.d2wmu0h.mongodb.net/courseBookingNewDatabase?retryWrites=true&w=majority" ,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);*/

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

// middlewares
// Allow all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included for all user routes in the "userRoutes" file.
app.use("/users", userRoutes);


app.use("/products", productRoutes);



const port = process.env.PORT || 4000

app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
})